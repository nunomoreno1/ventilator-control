# CS-STUDIO to design the dashboard of the ventilator

## Install Steps


1. Download and install Java JDK version 1.8.0_241.jdk (make sure the folder is int the path /Library/Java/JavaVirtualMachines/)
```https://www.oracle.com/java/technologies/javase-downloads.html#JDK8```

2. Download latest version of CS-Studio for your OS:
```http://download.controlsystemstudio.org/release/4.5/```
